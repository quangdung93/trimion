<?php

namespace App\Http\Controllers\Site;

use Arr;
use App\Models\Post;
use App\Models\Product;
use App\Models\Category;
use App\Models\PostCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index(){
        $newPosts = Post::active()->orderByDesc('created_at')->limit(5)->get();
        $home_section_news = theme('home_section_news');
        $listPostCategory = Arr::pluck($home_section_news, 'category_id');
        if($listPostCategory){
            $listNews = PostCategory::whereIn('id', $listPostCategory)->with('posts')->get();
        }
        return view('themes.kangen.homepage.index')->with([
            'newPosts' => $newPosts,
            'listNews' => $listNews ?? [],
        ]);
    }

    public function aboutPage(){
        return view('themes.kangen.pages.about')->with([
            'pageName' => 'about'
        ]);
    }

    public function handbookPage(){
        $listNews = Post::limit(20)->paginate(20);
        $productFeatured = Product::limit(5)->get();
        return view('themes.kangen.pages.handbook')->with([
            'listNews' => $listNews,
            'productFeatured' => $productFeatured
        ]);
    }
}
