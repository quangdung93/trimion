<?php

namespace App\Http\Controllers\Site;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Helpers\SchemaHelper;
use App\Services\PostService;
use App\Http\Controllers\Controller;
use App\Models\Product;

class PostController extends Controller
{
    protected $postService;

    public function __construct()
    {
        $this->postService = new PostService();
    }

    public function index(){
        $listNews = Post::limit(20)->paginate(20);
        $productFeatured = Product::limit(5)->get();
        return view('themes.kangen.posts.index')->with([
            'listNews' => $listNews,
            'productFeatured' => $productFeatured
        ]);
    }
    
    public function detail($post){
        $post->load('categories');

        //Handle content post
        $post->handleContentPost();
        //Get siderbar category
        $categorySiderbar = $this->postService->getCategorySiderBar();
        $postRelated = $this->postService->getPostRelated($post->categories->pluck('id')->toArray());
        
        //Schema
        $schema = SchemaHelper::schemaPost($post);
        
        return view('themes.kangen.posts.detail')
                ->with([
                    'metaData' => $this->getMetaData($post),
                    'schema' => $schema,
                    'post' => $post,
                    'categorySiderbar' => $categorySiderbar,
                    'postRelated' => $postRelated,
                ]);
    }

    public function getMetaData($model){
        return [
            'title' => $model->seo_title ?: $model->name,
            'description' => $model->meta_description,
            'keyword' => $model->meta_keywords,
            'image' => $model->image,
        ];
    }
}
