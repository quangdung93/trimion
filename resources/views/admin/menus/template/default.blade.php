<ul class="navbar-nav ml-auto {{ isset($children) ? 'sub-menu' : ''}}">
@foreach ($items as $item)
    <li class="nav-item {{ $item->children->isNotEmpty() ? 'dropdown' : '' }}">
        <a class="nav-link text-blue" href="{{ url($item->url) }}" target="{{ $item->target }}">
            <span>{{ $item->title }}</span>
            @if($item->children->isNotEmpty())
                <span class="icon-show"><i class="feather icon-chevron-down"></i></span>
            @endif
        </a>
        @if($item->children->isNotEmpty())
            {{-- @include('admin.menus.template.default', ['items' => $item->children, 'children' => true]) --}}
            <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <a class="dropdown-item" href="#">Something else here</a>
            </div>
        @endif
    </li>
@endforeach
    <li class="nav-item ml-5">
        <a class="nav-link text-blue header-btn-cart">
            <span class="cart-icon">
                <i class="fa fa-shopping-cart"></i>
                @php
                    $countCart = Cart::count();
                @endphp
                @if($countCart > 0)
                    <span>{{ $countCart }}</span>
                @endif
            </span>
            <div class="alert-cart">
                <div class="close"><i class="feather icon-x"></i></div>
                <div><i class="feather icon-check"></i> <span>Thêm giỏ hàng thành công!</span></div>
                <div id="view-popup-cart-header" class="btn bg-danger text-white mt-2">Xem giỏ hàng</div>
            </div>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-blue">
            <i class="fa fa-search"></i>
        </a>
    </li>
</ul>

{{-- <ul class="navbar-nav ml-auto">
    <li class="nav-item active">
        <a class="nav-link text-blue" href="#">Trang chủ</a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-blue" href="#">Về chúng tôi</a>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-blue" href="#" role="button" data-toggle="dropdown"
            aria-expanded="false">
            Sản phẩm
        </a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link text-blue">Cẩm nang</a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-blue">Tin tức</a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-blue">Hỗ trợ</a>
    </li>
    <li class="nav-item ml-5">
        <a class="nav-link text-blue">
            <i class="fa fa-shopping-cart"></i>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-blue">
            <i class="fa fa-search"></i>
        </a>
    </li>
</ul> --}}