@extends('themes.kangen.body')
@section('title', 'Trang chủ')
@section('content')
    {{-- Main Banner --}}
    @include('themes.kangen.components.main-banner')

    <section class="drop py-5">
        <div class="container">
            <div class="text-center">
                <h2 class="fs-5 text-blue">
                    Nước ion kiềm giàu hydro trim ion
                </h2>
                <h2 class="font-weight-bold fs-40 text-blue">
                    Không chỉ sạch mà còn tốt cho sức khỏe
                </h2>
            </div>

            <div class="row mt-4">
                <div class="col-lg-4 p-3 text-center">
                    <img src="{{ asset('themes/trimion/img/02.png') }}" class="img-fluid mb-3" alt="">
                    <p class="text-dark">
                        Lọc sạch theo tiêu chuẩn Nhật Bản
                        Làm sạch nước an toàn mà vẫn giữ lại các khoáng chất để điện phân
                    </p>
                </div>
                <div class="col-lg-4 p-3 text-center">
                    <img src="{{ asset('themes/trimion/img/01.png') }}" class="img-fluid mb-3" alt="">
                    <p class="text-dark">
                        Trung hòa Axit, ngăn ngừa dư thừa Axit. Cải thiện chức năng dạ dày, Cải thiện bệnh do thừa Axit
                        (gout, khớp,...)
                    </p>
                </div>
                <div class="col-lg-4 p-3 text-center">
                    <img src="{{ asset('themes/trimion/img/03.png') }}" class="img-fluid mb-3" alt="">
                    <p class="text-dark">
                        Khử chất oxi hóa, Khử gốc tự do
                        Chống lão hóa, Ngăn ngừa rủi ro bệnh tật, Thải độc cơ thể, Tăng sức đề kháng
                    </p>
                </div>
            </div>
        </div>

    </section>
    <!-- drop -->

    <!-- water -->
    <section class="water">
        <div class="container-fluid p-0">
            <img src="{{ asset('themes/trimion/img/water2.png') }}" class="img-fluid card-img-top" alt="">
        </div>
    </section>
    <!--water  -->

    <!-- cards -->
    <section class="cards py-5">
        <div class="container">
            <div class="text-center">
                <h1 class="font-weight-semibold text-blue">
                    CÔNG DỤNG <br>
                    Của nước ion kiềm giàu hydro Trim Ion
                </h1>

                <div class="row mt-4">
                    <div class="col-lg-4 p-3">
                        <div class="card border-0">
                            <div class="card-body py-5">
                                
                                <img src="{{ asset('themes/trimion/img/Component 275 – 1.png') }}" class="img-fluid mb-3" alt="">
                                <h3 class="playfair text-blue fs-32">
                                    Thải độc cơ thể
                                </h3>
                                <p>
                                    Due to the importance of water in our life we give 99.99% pure water to our
                                    customers.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 p-3">
                        <div class="card border-0">
                            <div class="card-body py-5">
                                <img src="{{ asset('themes/trimion/img/Component 276 – 1.png') }}" class="img-fluid mb-3" alt="">
                                <h3 class="playfair text-blue fs-32">
                                    Thải độc cơ thể
                                </h3>
                                <p>
                                    Due to the importance of water in our life we give 99.99% pure water to our
                                    customers.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 p-3">
                        <div class="card border-0">
                            <div class="card-body py-5">
                                <img src="{{ asset('themes/trimion/img/Component 277 – 1.png') }}" class="img-fluid mb-3" alt="">
                                <h3 class="playfair text-blue fs-32">
                                    Thải độc cơ thể
                                </h3>
                                <p>
                                    Due to the importance of water in our life we give 99.99% pure water to our
                                    customers.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 p-3">
                        <div class="card border-0">
                            <div class="card-body py-5">
                                <img src="{{ asset('themes/trimion/img/Component 278 – 1.png') }}" class="img-fluid mb-3" alt="">
                                <h3 class="playfair text-blue fs-32">
                                    Thải độc cơ thể
                                </h3>
                                <p>
                                    Due to the importance of water in our life we give 99.99% pure water to our
                                    customers.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 p-3">
                        <div class="card border-0">
                            <div class="card-body py-5">
                                <img src="{{ asset('themes/trimion/img/Component 279 – 1.png') }}" class="img-fluid mb-3" alt="">
                                <h3 class="playfair text-blue fs-32">
                                    Thải độc cơ thể
                                </h3>
                                <p>
                                    Due to the importance of water in our life we give 99.99% pure water to our
                                    customers.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 p-3">
                        <div class="card border-0">
                            <div class="card-body py-5">
                                <img src="{{ asset('themes/trimion/img/Component 280 – 1.png') }}" class="img-fluid mb-3" alt="">
                                <h3 class="playfair text-blue fs-32">
                                    Thải độc cơ thể
                                </h3>
                                <p>
                                    Due to the importance of water in our life we give 99.99% pure water to our
                                    customers.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- cards -->

    <!-- iframe -->
    <section class="iframe py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 p-3">
                    <div class="preview-intro-video">
                        <div class="youtube" data-embed="{{ get_embed_youtube(theme('home_section_10.link')) }}">
                            <div class="play-button"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 p-3">
                    <h2 class="fs-40 text-blue font-weight-bold">
                        {{ theme('home_section_10.title') }}
                    </h2>
                    <h4 class="fs-20 text-blue">
                        {{ theme('home_section_10.subtitle') }}
                    </h4>
                </div>
            </div>
        </div>
    </section>
    <!-- iframe -->

    <!-- slick -->
    <section class="slickSlider py-5">
        <div class="container">
            <div class="text-center">
                <h2 class="fs-40 font-weight-bold text-blue">
                    Sản phẩm
                </h2>
            </div>

            @php
                $section_1 = category(theme('home_section_2'));
                $products = $section_1->products;
            @endphp


            <div class="slider">

                @foreach($products as $product)
                <div class="slide p-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center">
                                <img src="{{ asset($product->image) }}" class="img-fluid" alt="{{ $product->name }}">
                            </div>
                            <h4 class="text-blue">
                               {{ $product->name }}
                            </h4>
                            <p>{!! nl2br($product->specification) !!}</p>
                            <div class="text-center">
                                <a href="{{ $product->link() }}" class="btn read-more">Xem thêm <i class="feather icon-chevrons-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                
            </div>
        </div>
    </section>
    <!-- slick -->


    <!-- bg blue -->
    <section class="bg-blue py-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 p-2 m-auto">
                    <img src="{{ asset('themes/trimion/img/slide-013.png') }}" class="img-fluid card-img-top" alt="">
                </div>
                <div class="col-lg-6 m-auto">
                    <h3 class="fs-30 text-blue font-weight-bold">
                        CHỌN TRIM ION
                    </h3>
                    <h4 class="fs-30 text-blue mb-5">
                        CHỌN NƯỚC TỐT CHO SỨC KHỎE
                    </h4>

                    <p class="text-blue">
                        <i class="fa fa-check"></i> TRIM ION thiết bị y tế được bộ y tế Nhật Bản chứng nhận
                    </p>
                    <p class="text-blue">
                        <i class="fa fa-check"></i> Giàu Hydro hòa tan và chỉ số chống oxy hóa (ORP) cao
                    </p>
                    <p class="text-blue">
                        <i class="fa fa-check"></i> Lượng khoáng tự nhiên trong nước tăng 20% sau quá trình điện phân
                    </p>
                    <p class="text-blue">
                        <i class="fa fa-check"></i> Cụm phân tử nước nhỏ - Độ thẩm thấu mạnh giúp thải độc cơ thể
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- bg blue -->



    <!-- bg blue -->
    <section class="bg-blue">
        <div class="container-fluid p-0">
            <img src="{{ asset('themes/trimion/img/Group 15280.png') }}" class="img-fluid card-img-top" alt="">
        </div>
    </section>
    <!-- bg blue -->

    <!-- bottom slider -->
    <section class="bottom-slider py-5">
        <div class="container">
            <div class="text-center">
                <h2 class="fs-40 font-weight-bold text-blue">
                    KHÁCH HÀNG ĐÃ NÓI GÌ VỀ TRIM ION?
                </h2>
            </div>

            @php
                $youtube = array_values(theme('home_section_youtube'));
            @endphp

            <div class="slider">
                @if(count($youtube) > 0)
                    @foreach($youtube as $value)
                    <div class="slide p-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="text-center">
                                    <div class="preview-intro-video">
                                        <div class="youtube" data-embed="{{ get_embed_youtube($value['link']) }}">
                                            <div class="play-button"></div>
                                        </div>
                                    </div>
                                </div>
                                <h4 class="text-blue mt-3"><div class="youtube-title">{{ $value['title'] }}</div></h4>
                                <p>{{ $value['subtitle'] }}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif

            </div>
        </div>
    </section>
    <!-- bottom slider -->


    <!-- bg blue -->
    <section class="bg-blue py-5">
        <div class="container">
            <div class="text-center">
                <h2 class="fs-40 font-weight-bold text-blue">
                    Bài viết nổi bật
                </h2>
            </div>
            <div class="slider">
                @if($listNews)
                    @foreach($listNews[0]->posts as $value)
                    <div class="slide p-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="text-center">
                                    <a href="{{ url($value->link()) }}">
                                        <img class="img-fluid" src="{{ asset($value->image) }}" alt="{{ $value['name'] }}"/>
                                    </a>
                                </div>
                                <h4 class="text-blue mt-3">
                                    <a href="{{ url($value->link()) }}"><div class="youtube-title">{{ $value['name'] }}</div></a>
                                </h4>
                                <p>{!! post_excerpt($value['body']) !!}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif

            </div>
        </div>
    </section>

@endsection

@section('javascript')
<script>
    $(document).ready(function(){
        //Youtube
        if($('.youtube').length > 0){
            generateYoutube();
        }

        $('.youtube-item').on('click', function(e){
            e.preventDefault();
            $('.youtube-item').removeClass('active');
            $(this).addClass('active');
            let embed = $(this).find('.video-thumb .youtube-list').data('embed'),
                youtube = renderIframeYoutube(embed);
            $('.section-youtube .preview-intro-video .youtube').html(youtube);
        });

        function generateYoutubeLazyLoad() {
            var youtube = document.querySelectorAll('.youtube');
            for (var i = 0; i < youtube.length; i++) {
                // thumbnail image source.
                var source = "https://img.youtube.com/vi/" + youtube[i].dataset.embed + "/hqdefault.jpg"; //sddefault.jpg
                // Load t image asynchronously    
                var image = new Image();
                image.setAttribute("class", "lazy");
                image.setAttribute("src", source);
                image.addEventListener("load", function () {
                    youtube[i].appendChild(image);
                    youtubeLazyLoad(youtube[i].querySelectorAll('.lazy'));
                }(i));
            }
        }

        function youtubeLazyLoad(element, timeout = 0) {
            setTimeout(function () {
                $(element).lazyload().addClass('youtube-loaded');
            }, timeout);
        }

        function renderIframeYoutube(embed){
            let iframe = document.createElement("iframe");
                iframe.setAttribute("frameborder", "0");
                iframe.setAttribute("class", "youtube-video");
                iframe.setAttribute("width", "100%");
                iframe.setAttribute("height", "100%");
                iframe.setAttribute("allowfullscreen", "");
                iframe.setAttribute("src", "https://www.youtube.com/embed/" + embed + "?rel=0&showinfo=0&autoplay=1");
                return iframe;
        }
    });
</script>
@endsection