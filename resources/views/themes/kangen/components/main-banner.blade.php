{{-- <div class="main-banner">
    <div class="container">
        <div class="row">
            <div class="content-wrap">
                <div class="item left">
                    <div class="title">{{ theme('home_banner.title') }}</div>
                    <div class="content">{{ theme('home_banner.content') }}</div>
                    <a href="{{ theme('home_banner.link') }}" class="btn read-more">Xem thêm <i class="feather icon-chevrons-right"></i></a>
                </div>
                <div class="item right">
                    <img class="img-fluid" src="{{ asset(theme('home_banner.image')) }}" alt="Banner main"/>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<section class="banner py-5">
    <div class="container">
        <div class="text-center">
            <h2 class="fs-5 text-blue">
                ĐÓN ĐẦU XU HƯỚNG THẾ GIỚI
            </h2>
            <h2 class="font-weight-bold fs-40 text-blue">
                Công nghệ thế hệ 4 của ngành lọc nước
            </h2>
        </div>
    </div>
    <div class="container-fluid p-0">
        <img src="{{ asset('themes/trimion/img/banner.png') }}" class="img-fluid card-img-top" alt="">
    </div>
</section>