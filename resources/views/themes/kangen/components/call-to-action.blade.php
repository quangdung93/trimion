<section class="call-to-action">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="left">
                    <div class="icon-hand-oke"><img class="lazy" width="50" data-src="{{ asset('themes/kangen/images/hand-two-finger.svg') }}" alt="Icon oke"/></div>
                    <div class="content">
                        <h4 class="text-white">{{ theme('home_section_10.title') }}</h4>
                        <p class="text-white m-0">{{ theme('home_section_10.subtitle') }}</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <a href="{{ theme('home_section_10.link') }}" class="btn read-more mt-4"><i class="feather icon-chevrons-right"></i> Tìm hiểu thêm</a>
            </div>
        </div>
    </div>
</section>