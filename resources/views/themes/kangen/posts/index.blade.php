@extends('themes.kangen.body')
@section('title', 'Tin tức')
@section('content')
<div class="container py-5">
    <div class="row">
        <div class="col-lg-12">
            <div class="d-flex align-items-center">
                <a href="/" class="text-blue d-inline-block mr-2">Trang chủ</a>
                &raquo;
                <a href="/san-pham" class="text-blue d-inline-block mx-2">
                    Tin tức
                </a>
            </div>
        </div>
    </div>
</div>
    <div class="news-page mt-3">
        <div class="container">
            <div class="news-wrapper">
                <div class="row">
                    <div class="col-sm-9">
                        @if($listNews)
                            @foreach($listNews as $key => $value)
                            <div class="news-item">
                                <div class="image">
                                    <img src="{{ asset($value->image) }}" alt="">
                                </div>
                                <div class="info">
                                    <a href="{{ $value->link() }}"><h4>{{ $value->name }}</h4></a>
                                    <div class="subcontent">{!! post_excerpt($value['body']) !!}</div>
                                    <div class="created-at">{{ \Carbon\Carbon::parse($value['created_at'])->format('d-m-Y') }}</div>
                                </div>
                            </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="col-sm-3 siderbar-news">
                        <h4 class="color-main">Sản Phẩm Nổi Bật</h4>
                        <div class="product-featured">
                        @foreach($productFeatured as $key => $product)
                            <div class="product-featured-item">
                                <div class="image">
                                    <img src="{{ asset($product->image) }}" alt="">
                                </div>
                                <div class="info">
                                    <a href="{{ $product->link() }}"><h4 class="color-main">{{ $product->name }}</h4></a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            {{-- {!! $products->render('themes.kangen.components.pagination') !!} --}}
        </div>
    </div>
@endsection

@section('javascript')
<script>
    $(document).ready(function(){
        
    });
</script>
@endsection