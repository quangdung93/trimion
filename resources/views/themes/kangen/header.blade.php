@php
    $path = request()->path();
    $tempPath = explode('/', $path);
    $pathName = $tempPath[0];
    if(in_array($pathName, [ABOUT_URL, PRODUCT_URL, HANDBOOK_URL])){
        $backgroundImage = 'bg-about.png';
    }
    else{
        $backgroundImage = 'bg.png';
    }

    switch ($pathName) {
        case ABOUT_URL:
            $config = [
                'name' => "Về chúng tôi",
                'background' => "bg-about.png",
            ];
            break;
        case PRODUCT_URL:
            $config = [
                'name' => "Sản phẩm",
                'background' => "bg-about.png",
            ];
            break;
        case NEWS_URL:
            $config = [
                'name' => "Tin tức",
                'background' => "bg-about.png",
            ];
            break;
        case HANDBOOK_URL:
            $config = [
                'name' => "Cẩm nang",
                'background' => "bg-about.png",
            ];
            break;
        case CHECKOUT_URL:
            $config = [
                'name' => "Thanh toán",
                'background' => "bg-about.png",
            ];
            break;
        default:
            $config = [
                'name' => "Thanh toán",
                'background' => "bg.png",
            ];
            break;
    }

    $classHeader = 
    $pathName == PRODUCT_URL && count($tempPath) > 1 ||
    $pathName == 'checkout'
    ? 'header-product-detail' : '';
@endphp

<header class="py-2 {{ $classHeader }}" style="background-image: url('{{ asset('themes/trimion/img/'.$config['background']) }}')">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-transparent">
            <a class="navbar-brand" href="/">
                <img src="{{ asset(theme('logo.image')) }}" class="img-fluid logo" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                {!! menu('main-menu') !!}
            </div>
        </nav>
    </div>
    
    @if(in_array($pathName, [ABOUT_URL, PRODUCT_URL, HANDBOOK_URL, NEWS_URL]) && count($tempPath) == 1)
    <div class="container mt-5 pt-5">
        <div class="row">
            <div class="col-lg-6">
                <h1 class="playfair font-weight-bold">
                    {{ $config['name'] }}
                </h1>
                <div class="d-flex align-items-center">
                    <a href="">
                        <i class="fa fa-home text-blue mr-2"></i>
                    </a>
                    <a href="" class="text-dark">
                        Trang chủ
                    </a>
                    <i class="fa fa-angle-right mx-2"></i>
                    <a href="" class="text-dark">
                        {{ $config['name'] }}
                    </a>
                </div>
            </div>
        </div>
    </div>
    @elseif(request()->path() == '/')
    <div class="container mt-5 pt-5">
        <div class="row">
            <div class="col-lg-6 offset-md-2">
                <div class="card border-0 bg-white-50 rounded-0">
                    <div class="card-body text-center fs-40 text-blue">
                        Không chỉ sạch
                        mà còn tốt cho sức khỏe
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</header>