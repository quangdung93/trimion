@extends('themes.kangen.body')
@section('title', 'Sản phẩm')
@section('schema')
    @if(!empty($schema))
        <script type="application/ld+json">
            {!! $schema !!}
        </script>
    @endif
@endsection
@section('content')
    {{-- <nav aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="{{ url($product->categories[0]->link()) }}">{{ $product->categories[0]->name }}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $product->name }}</li>
            </ol>
        </div>
    </nav> --}}
    <!-- bredcrumb -->
    <div class="container py-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="d-flex align-items-center">
                    <a href="/" class="text-blue d-inline-block mr-2">Trang chủ</a>
                    &raquo;
                    <a href="/san-pham" class="text-blue d-inline-block mx-2">
                        Sản phẩm
                    </a>
                    &raquo;
                    <a href="" class="text-blue d-inline-block ml-2">
                        {{ $product->name }}
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- bredcrumb -->
    <!-- slider gallery -->
    <section class="gallery pb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 p-2 border">
                    <div class="row">
                        <div class="col-lg-6 py-2">
                            <div class="slider-for">
                                <div class="slide">
                                    <img src="{{ asset($product->image) }}" class="img-fluid card-img-top" alt="{{ $product->name }}">
                                </div>
                            </div>
                            <div class="slider-nav">
                                <div class="slide">
                                    <img src="{{ asset($product->image) }}" class="img-fluid" alt="{{ $product->name }}">
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 py-2 px-3">
                            <h2 class="font-weight-bold text-blue fs-30">
                                {{ $product->name }}
                            </h2>
                            <div class="d-flex justify-content-between align-items-center">
                                <p class="mb-0">
                                    5.0
                                    <i class="fa fa-star text-danger"></i>
                                    <i class="fa fa-star text-danger"></i>
                                    <i class="fa fa-star text-danger"></i>
                                    <i class="fa fa-star text-danger"></i>
                                    <i class="fa fa-star text-danger"></i>
                                </p>
                                <span>
                                    |
                                </span>
                                <p class="mb-0">
                                    50 Đánh giá
                                </p>
                                <span>
                                    |
                                </span>
                                <p class="mb-0">
                                    99 Đã bán
                                </p>
                            </div>

                            <h2 class="bg-light p-2 my-3">
                                <span class="text-blue fs-40 font-weight-bold">{{ number_format($product->price) }} đ</span>
                            </h2>
                            <p>
                                {{ $product->meta_description }}
                            </p>
                            <ul class="mx-0 pl-3">
                                <li>Thương hiệu: {{ $product->brand->name }}</li>
                                <li>Xuất xứ : {{ $product->origin ?: 'Nhật Bản' }}</li>
                                <li>Thông số kỹ thuật: </br> {!! nl2br($product->specification) !!}</li>
                            </ul>

                            <p>Số lượng</p>
                            <form data-action="{{ route('cart.add') }}">
                                <div class="add-to-cart">
                                    <div class="qty-box input-group w-50 mb-3">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <input type="hidden" name="product_id" value="{{ $product->id }}" />
                                        <a href="#" class="btn btn-outline-info rounded-0 border-right-0 btn-cart-minus"><i class="fa fa-minus"></i></a>
                                        <input name="qty" class="qty-input form-control border-info text-center rounded-0" type="number" value="1" min="1" max="99" maxlength="2"/>
                                        <a href="#" class="btn btn-outline-info rounded-0 border-left-0 btn-cart-plus"><i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-info btn-addtocart"><i class="fa fa-plus mr-4"></i> Chọn Mua</a>
                                <a href="#" class="btn btn-outline-info"><i class="fa fa-heart mr-4"></i> Yêu Thích</a>
                            </form>
                            <div class="d-flex mt-4">
                                <p class="mb-0">
                                    Chia sẻ:
                                </p>
                                <ul class="list-unstyled d-flex justify-content-end align-items-center">
                                    <li class="ml-3 icon-holder">
                                        <a href="" class="text-dark text-decoration-none">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li class="ml-3 icon-holder">
                                        <a href="" class="text-dark text-decoration-none">
                                            <i class="fab fa-instagram"></i>
                                        </a>
                                    </li>
                                    <li class="ml-3 icon-holder">
                                        <a href="" class="text-dark text-decoration-none">
                                            <i class="fab fa-linkedin-in"></i>
                                        </a>
                                    </li>
                                    <li class="ml-3 icon-holder">
                                        <a href="" class="text-dark text-decoration-none">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 py-2 px-4 border">
                    <div class="d-flex align-items-center">
                        <i class="fa fa-phone fs-32 mr-2 text-blue"></i> <span class="text-dark fs-32">Hotline</span>
                    </div>
                    <p class="fs-32 text-warning">
                        0904.005.440
                    </p>
                    <p class="fs-24">
                        Chúng tôi cam kết
                    </p>
                    <p class="">
                        <i class="fa fa-circle text-warning"></i> Lắp đặt chuyên nghiệp
                    </p>
                    <p class="">
                        <i class="fa fa-circle text-warning"></i> Hậu mãi chu đáo
                    </p>
                    <p class="">
                        <i class="fa fa-circle text-warning"></i> Dịch vụ uy tín
                    </p>
                    <p class="">
                        <i class="fa fa-circle text-warning"></i> Sản phẩm chính hãng
                    </p>
                    <p class="">
                        <i class="fa fa-circle text-warning"></i> Bảo hành 2 năm
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- slider gallery -->

    <section class="bordered pb-5">
        <div class="container border">
            <div class="py-4">
                <div class="btns d-flex justify-content-between align-items-center">
                    <div class="btn btn1 fieldBtn btn-outline-info active px-5 py-3 rounded-0 mx-2">
                        Tổng quan
                    </div>
                    <div class="btn btn2 fieldBtn btn-outline-info px-3 py-3 rounded-0 mx-2">
                        Tính năng
                    </div>
                    <div class="btn btn3 fieldBtn btn-outline-info px-3 py-3 rounded-0 mx-2">
                        Thông số KT
                    </div>
                    <div class="btn btn4 fieldBtn btn-outline-info px-3 py-3 rounded-0 mx-2">
                        Chứng nhận
                    </div>
                    <div class="btn btn5 fieldBtn btn-outline-info px-3 py-3 rounded-0 mx-2">
                        Cảm nhận KH
                    </div>
                    <div class="btn btn6 fieldBtn btn-outline-info px-3 py-3 rounded-0 mx-2">
                        Câu hỏi thường gặp
                    </div>
                </div>
                <div class="content-detail">
                    {!! $product->content !!}
                </div>
            </div>
        </div>
    </section>
    <!-- bordered section -->


    <!-- banner -->
    <section class="">
        <div class="container-fluid px-0">
            <img src="/themes/trimion/img/may-loc-nuoc-ion-kiem-trim-ion-hyper-0001.png" class="img-fluid card-img-top" alt="">
        </div>
    </section>
    <!-- banner -->

    <!-- row last -->
    <section class="rowLast py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 p-2">
                    <h2 class="fs-40 font-weight-bold text-blue">
                        Máy lọc nước Trim Ion Hyper được trang bị công nghệ tiên tiến nhất
                    </h2>

                    <ul class="mx-0 p-0 pl-3">
                        <li>
                            Bộ lõi lọc tinh cao cấp:
                        </li>
                        <li>
                            Công nghệ điện phân kép:
                        </li>
                        <li>
                            Công nghệ tự động đảo chiều điện cực:
                        </li>
                        <li>
                            Tấm điện cực của máy Trim ion Hyper bằng chất liệu Titan phủ Platinum
                        </li>
                        <li>
                            Máy lọc nước Trim Ion Hyper cho ra nước kiềm đạt chỉ số ORP cao
                        </li>

                    </ul>
                </div>

                <div class="col-lg-6 p-2">
                    <img src="/themes/trimion/img/cong-nghe-dien-phan-kep-trim-ion-grace2.png" class="img-fluid card-img-top" alt="">
                </div>
            </div>
        </div>
    </section>
@endsection

@section('javascript')
<script>
    $(document).ready(function(){
        $('.content-detail img').each(function () {
            $(this).addClass('lazy');
            $(this).lazyload();
        });
    });
</script>
@endsection