@extends('themes.kangen.body')
@section('title', 'Sản phẩm')
@section('content')
    <div class="product-page mt-3">
        <div class="container">
            {{-- <div class="page-title">
                <h3>Sản Phẩm</h3>
                <p class="text-center">Các dòng sản phẩm của tập đoàn ENAGIC được nhập khẩu bởi công ty Kangen Việt Nam</p>
            </div> --}}
            <div class="products-wrapper">
                {!! product_template($products) !!}
            </div>
            {{-- {!! $products->render('themes.kangen.components.pagination') !!} --}}
        </div>
    </div>
@endsection

@section('javascript')
<script>
    $(document).ready(function(){
        
    });
</script>
@endsection