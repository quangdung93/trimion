@extends('themes.kangen.body')
@section('title', 'Trang chủ')
@section('content')

    <!-- iframe -->
    <section class="iframe py-5">
        <div class="container">
            <div class="row">
                @php
                    $about_section_1 = theme('about_section_1')[0];
                @endphp
                <div class="col-lg-6 p-3 position-relative">
                    <img src="img/dot_shapes.png" class="img-fluid dots" alt="">
                    <div class="preview-intro-video">
                        <div class="youtube" data-embed="{{ get_embed_youtube($about_section_1['link']) }}">
                            <div class="play-button"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 p-3">
                    <p class="mb-5">
                        <i class="fa fa-square text-blue"></i> {{ $about_section_1['title'] }}
                    </p>
                    <h2 class="fs-32 text-blue font-weight-bold">
                        {{ $about_section_1['subtitle'] }}
                    </h2>
                    <div class="border-left border-info border-3 pl-4">
                        <p>
                            {{ $about_section_1['subtitle_2'] }}
                        </p>
                    </div>
                    <table class="table">
                        <tr>
                            <td class="border-0">
                                <i class="fa fa-map-marker-alt text-blue"></i>
                            </td>
                            <td class="border-0">
                                Địa chỉ: 214 Hoàng Hoa Thám, P. 12, Quận Tân Bình, TP. HCM
                            </td>
                        </tr>
                        <tr>
                            <td class="border-0">
                                <i class="fa fa-phone text-blue"></i>
                            </td>
                            <td class="border-0">
                                Điện thoại: 0904 005 440
                            </td>
                        </tr>
                        <tr>
                            <td class="border-0">
                                <i class="fa fa-envelope text-blue"></i>
                            </td>
                            <td class="border-0">
                                Email: trim_ion@gmail.com
                            </td>
                        </tr>
                        <tr>
                            <td class="border-0">
                                <i class="fa fa-globe text-blue"></i>
                            </td>
                            <td class="border-0">
                                Website: trim_ion.vn
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- iframe -->


    <!-- overlay -->
    <section class="overlay py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <p class="fs-24 text-info">
                        123-456-7890/call
                    </p>
                    <h2 class="fs-40 font-weight-bold text-blue">
                        Chọn Trim ion
                        chọn sức khỏe gia đình
                    </h2>
                    <button class="btn btn-info rounded-0 px-4 mt-4">
                        Get Appointment
                    </button>
                </div>
            </div>
        </div>
    </section>
    <!-- overlay -->


    <!-- boxes -->
    <section class="boxes py-5">
        <div class="container">
            <div class="text-center">
                <p class="text-warning fs-18">
                    <i class="fa fa-square"></i> Our Expert Worker
                </p>
                <h2 class="fs-64 text-blue font-weight-bold">
                    Tại sao chọn TRIM ION
                </h2>
            </div>

            <div class="row mt-4">
                <div class="col-lg-4 p-3">
                    <div class="card h-100 border-0 rounded-0 shadow">
                        <div class="card-body p-5">
                            <i class="fa fa-droplet fs-40 text-info"></i>
                            <h2 class="fs-32 mt-3">Chuyên nghiệp</h2>
                            <p>
                                over 1 million+ homes for sale available
                                on the website, we can match you with a
                                house you will want to call home.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 p-3">
                    <div class="card h-100 border-0 rounded-0 shadow">
                        <div class="card-body p-5">
                            <i class="fa fa-person fs-40 text-info"></i>
                            <h2 class="fs-32 mt-3">Chính hãng Nhật Bản</h2>
                            <p>
                                over 1 million+ homes for sale available
                                on the website, we can match you with a
                                house you will want to call home.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 p-3">
                    <div class="card h-100 border-0 rounded-0 shadow">
                        <div class="card-body p-5">
                            <i class="fa fa-thumbs-up fs-40 text-info"></i>
                            <h2 class="fs-32 mt-3">Tin cậy</h2>
                            <p>
                                over 1 million+ homes for sale available
                                on the website, we can match you with a
                                house you will want to call home.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 p-3">
                    <div class="card h-100 border-0 rounded-0 shadow">
                        <div class="card-body p-5">
                            <i class="fa fa-screwdriver-wrench fs-40 text-info"></i>
                            <h2 class="fs-32 mt-3">Bảo hành uy tín</h2>
                            <p>
                                over 1 million+ homes for sale available
                                on the website, we can match you with a
                                house you will want to call home.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 p-3">
                    <div class="card h-100 border-0 rounded-0 shadow">
                        <div class="card-body p-5">
                            <i class="fa fa-flask-vial fs-40 text-info"></i>
                            <h2 class="fs-32 mt-3">Kinh nghiệm</h2>
                            <p>
                                over 1 million+ homes for sale available
                                on the website, we can match you with a
                                house you will want to call home.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 p-3">
                    <div class="card h-100 border-0 rounded-0 shadow">
                        <div class="card-body p-5">
                            <i class="fa fa-recycle fs-40 text-info"></i>
                            <h2 class="fs-32 mt-3">Giải pháp hoàn hảo</h2>
                            <p>
                                over 1 million+ homes for sale available
                                on the website, we can match you with a
                                house you will want to call home.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- boxes -->

    <!-- full iframe -->
    <section class="iframe-full h-50">
        <div class="container-fluid p-0">
            @php
                $about_section_2 = theme('about_section_2')[0];
            @endphp
            {{-- <div class="preview-intro-video">
                <div class="youtube" data-embed="{{ get_embed_youtube($about_section_2['link']) }}">
                    <div class="play-button"></div>
                </div>
            </div> --}}
            <iframe src="https://www.youtube.com/embed/D0UnqGm_miA" title="YouTube video player" frameborder="0"
                class="h-100 w-100"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
        </div>
    </section>
    <!-- full iframe -->


    <!-- testimonial -->
    <section class="testimonial py-5">
        <div class="container">
            <div class="text-center">
                <p class="text-blue fs-18">
                    <i class="fa fa-square"></i> Working Process
                </p>
                <h2 class="fs-64 text-blue font-weight-bold">
                    Khách Hàng Nói Về Trim Ion
                </h2>
            </div>

            <div class="row mt-4">
                <div class="col-lg-6 p-3">
                    <div class="card border-0 rounded-0 shadow">
                        <div class="card-body p-5">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex align-items-start mt-3">
                                    <img src="img/image - bg.png" alt="" class="img-fluid rounded-circle" width="50">
                                    <div class="ml-3">
                                        <h5 class="fs-18 font-weight-bold">John Doe</h5>
                                        <p class="fs-14 text-muted">CEO, Company</p>
                                    </div>
                                </div>
                                <i class="fa fa-quote-left fs-40 text-info"></i>
                            </div>
                            <p class="fs-18 mt-3">
                                over 1 million+ homes for sale available
                                on the website, we can match you with a
                                house you will want to call home.
                            </p>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 p-3">
                    <div class="card border-0 rounded-0 shadow">
                        <div class="card-body p-5">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex align-items-start mt-3">
                                    <img src="img/image - bg.png" alt="" class="img-fluid rounded-circle" width="50">
                                    <div class="ml-3">
                                        <h5 class="fs-18 font-weight-bold">John Doe</h5>
                                        <p class="fs-14 text-muted">CEO, Company</p>
                                    </div>
                                </div>
                                <i class="fa fa-quote-left fs-40 text-info"></i>
                            </div>
                            <p class="fs-18 mt-3">
                                over 1 million+ homes for sale available
                                on the website, we can match you with a
                                house you will want to call home.
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- testimonial -->

@endsection

@section('javascript')
<script>
    $(document).ready(function(){
        //Youtube
        if($('.youtube').length > 0){
            generateYoutube();
        }

        $('.youtube-item').on('click', function(e){
            e.preventDefault();
            $('.youtube-item').removeClass('active');
            $(this).addClass('active');
            let embed = $(this).find('.video-thumb .youtube-list').data('embed'),
                youtube = renderIframeYoutube(embed);
            $('.section-youtube .preview-intro-video .youtube').html(youtube);
        });

        function generateYoutubeLazyLoad() {
            var youtube = document.querySelectorAll('.youtube');
            for (var i = 0; i < youtube.length; i++) {
                // thumbnail image source.
                var source = "https://img.youtube.com/vi/" + youtube[i].dataset.embed + "/hqdefault.jpg"; //sddefault.jpg
                // Load t image asynchronously    
                var image = new Image();
                image.setAttribute("class", "lazy");
                image.setAttribute("src", source);
                image.addEventListener("load", function () {
                    youtube[i].appendChild(image);
                    youtubeLazyLoad(youtube[i].querySelectorAll('.lazy'));
                }(i));
            }
        }

        function youtubeLazyLoad(element, timeout = 0) {
            setTimeout(function () {
                $(element).lazyload().addClass('youtube-loaded');
            }, timeout);
        }

        function generateYoutube() {
            var youtube = document.querySelectorAll(".youtube");
            for (var i = 0; i < youtube.length; i++) {
                // thumbnail image source.
                var source = "https://img.youtube.com/vi/" + youtube[i].dataset.embed + "/hqdefault.jpg"; //sddefault.jpg
                // Load t image asynchronously    
                var image = new Image();
                image.src = source;
                image.addEventListener("load", function () {
                    youtube[i].appendChild(image);
                }(i));
                youtube[i].addEventListener("click", function () {
                    var iframe = document.createElement("iframe");
                    iframe.setAttribute("frameborder", "0");
                    iframe.setAttribute("class", "youtube-video");
                    iframe.setAttribute("allowfullscreen", "");
                    iframe.setAttribute("src", "https://www.youtube.com/embed/" + this.dataset.embed + "?rel=0&showinfo=0&autoplay=1");
                    this.innerHTML = "";
                    this.appendChild(iframe);
                });
            }
        }

        function renderIframeYoutube(embed){
            let iframe = document.createElement("iframe");
                iframe.setAttribute("frameborder", "0");
                iframe.setAttribute("class", "youtube-video");
                iframe.setAttribute("width", "100%");
                iframe.setAttribute("height", "100%");
                iframe.setAttribute("allowfullscreen", "");
                iframe.setAttribute("src", "https://www.youtube.com/embed/" + embed + "?rel=0&showinfo=0&autoplay=1");
                return iframe;
        }
    });
</script>
@endsection