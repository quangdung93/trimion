<footer class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 p-2">
                <div class="row">
                    <div class="col-lg-4 py-2">
                        {!! shortcode('footer_1') !!}
                    </div>
                    <div class="col-lg-4 py-2">
                        {!! shortcode('footer_2') !!}
                    </div>
                    <div class="col-lg-4 py-2">
                        {!! shortcode('footer_3') !!}
                    </div>
                </div>
            </div>
            <div class="col-lg-6 p-2">
                <div class="row">
                    <div class="col-lg-6 py-2">
                        {!! shortcode('footer_4') !!}
                    </div>
                    <div class="col-lg-6 py-2">
                        <div class="text-right">
                            <p class="text-white font-weight-bold">
                                Kết nối với chúng tôi:
                            </p>
                        </div>
                        <ul class="list-unstyled d-flex justify-content-end align-items-center">
                            <li class="ml-3 icon-holder">
                                <a href="" class="text-dark text-decoration-none">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li class="ml-3 icon-holder">
                                <a href="" class="text-dark text-decoration-none">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                            <li class="ml-3 icon-holder">
                                <a href="" class="text-dark text-decoration-none">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
                            <li class="ml-3 icon-holder">
                                <a href="" class="text-dark text-decoration-none">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <div class="mt-5">
            <div class="text-center">
                {!! shortcode('copyright') !!}
            </div>
        </div>
    </div>
</footer>

{{--  Popup Cart --}}
@include('themes.kangen.cart.popup')